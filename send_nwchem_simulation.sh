#!/bin/sh
# Víctor Gómez González, 2016
#SBATCH -N 1
#SBATCH -n 24
#SBATCH -p sun
#SBATCH -J Na4_opt
#SBATCH -e salida.nwo
#SBATCH -o salida.nwo
#SBATCH -x SUN011,SUN013

mpirun nwchem Na4_optimization.nw
