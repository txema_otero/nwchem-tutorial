# NWChem Tutorial
- - - -

We are going to perform a DFT simulation of a cluster of Sodium with 4 atoms.
The chosen software is NWChem. See documentation in [NWChem
Documentation](http://www.nwchem-sw.org/index.php/Release66:NWChem_Documentation).

Geometry Optimization
=====================

The first step is to calculate the optimal atomic distribution. The output of
this optimization can be used to perform energy, raman, dipolar... calculations.
We follow the next steps.

- We create the geometry file.

    We have to have the configuration of the atoms that we want to simulate in a
    `.xyz` file. The format of this file must be:
        * First line: Number of atoms
        * Second line: Comment line
        * Atoms lines: The lines with four columns: atom_name, x_pos, y_pos, z_pos

    See the [example](./Na4.xyz) file.

- We create the file with the options:

    These files are equivalent to `.mdp` files in gromacs. The extension is
    `.nw`. The description of the main option are commented in
    [options](./Na4_optimization.nw) file file. For more information see the
    [documentation](http://www.nwchem-sw.org/index.php/Release66:NWChem_Documentation).

- To run the calculation, the [sbatch script](./send_nwchem_simulation.sh) is
    executed with:

    ``` text
        sbatch send_nwchem_simulation.sh
    ```

    Options there:

        - N = 1 : Number of machines
        - n = 24 : Number of cores
        - p = sun : Name of the Queue
        - J = Na4_opt : Name of the job
        - e = salida.nwo : Output file for the error messages
        - o = salida.nwo : Output file for the info messages
        - x = SUN011,SUN013 : Exclude machines
        
__Caution:__ As the number of atoms increases, the memory consumed by the
process increases exponentially, e.g. for 80 atoms the required RAM is about
12Gb and the software decide to work with disk memory.

Partial Charges
=====================

To calculate the partial charges, we have to put in the
[options](./Na4_optimization.nw) file the block **esp** and run **task esp**:

``` text
    esp
    recalculate
    end

    task dft
    task esp
```

When it is run it will generate a `.q` file with the partial charges for each
atom. 

__Note:__ See *Electrostatic potentials* section in the
[documentation](http://www.nwchem-sw.org/index.php/Release66:NWChem_Documentation)
for more information.

TD-DFT
=====================

We have to include the **tddft** block in the `.nw` options file. An example of
this file is provided [here](./tddft.nw). The main options are:


- rpa : random phase approximation
- nroots : Number of exited states
- maxvecs : Maximum number of ...
- singlet : Calculates the singlet solutions 
- triplet : Calculates the triplet solutions 

With this configuration file we run linear responsive TDDFT (LR-TDDFT). 

__Note:__ See page 149 in the
[documentation](http://www.nwchem-sw.org/index.php/Release66:NWChem_Documentation)
for more information.

Optical Spectra
=====================

With the real time TDDFT (RT-TDDFT) we can calculate the Raman spectra. To do
that we need to run a simulation with the options file
[optical_spectra](./optical_spectra.nw). There we can define the shape of the
incident wave. Then, in the output we will find the spectra of the output
radiation (file `.nwo`). 

__Note : This is a very time consuming simulation.__

Raman
=======

In the load option of `.nw` [file](./Na4_optimization.nw) you have to specify
the optimized `.xyz` file or add the line:
``` text
  task dft raman
```
after the `dft optimize` task. 

The output file of this task has the `.normal` extension and basically contains
two columns with the frequencies and the height of the peaks. See the
[example](molecule.normal) file.

__Note : Also time consuming simulation.__

# Examples
- - - -

