#!/bin/sh
# Víctor Gómez González, 2016
#SBATCH -N 42
#SBATCH -n 1008
#SBATCH --ntasks-per-node=24
#SBATCH -p thinnodes
#SBATCH -J optimization
#SBATCH -t 100:00:00
#SBATCH -e salida.nwo
#SBATCH -o salida.nwo
#SBATCH --mem-per-cpu=5GB


module load gcc/5.3.0 impi mkl/11.3 python/2.7.11

ulimit -s unlimited

mpirun $HOME/nwchem_src/nwchem-6.6/bin/LINUX64/nwchem optimization.nw



